/**
\page ArmarXCore-Tutorials-debugging Debugging Tutorial

\section ArmarXCore-Tutorials-debugging-commandline Commandline debugging with GDB

To debug ArmarX applications you can use GDB in the following way:

\code
gdb --args /path/to/binary --Ice.Config=/path/to/config/config.cfg
\endcode

\warning Please make sure to start gdb in the foreground. Otherwise it is impossible to debug the executable correctly.

After gdb has finished loading the program it can be executed by typing 'run' into the
gdb console window.
This will start executing the program (a bit slower than usual)
and stop if a segmentation fault occurs.

Here is a list of the most used gdb commands:

\li 'bt' display the stack of the current thread
\li 'up' go one frame up on the stack (increase the frame number)
\li 'down' go one frame down on the stack (decrease the frame number)
\li 'p var' display the contents of variable var
\li 'p *var' display the contents of the element var points to


\section ArmarXCore-Tutorials-debugging-qtcreator Debugging with QtCreator

\subsection Setup

Debugging with QtCreator is supported.
Depending on your setup you may have to allow the QtCreator debugger to attach to external programs.
If you try to attach to an ArmarXComponent and you get a message like "ptrace: Operation not permitted." you need to
execute the following command before starting the program (sudo rights needed, taken from https://bugreports.qt-project.org/browse/QTCREATORBUG-3509):
\code
echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
\endcode
Note that after rebooting the value will be reverted to 1. You can add this line to a startup script (or figure out a permanant solution).


\subsection ArmarXCore-Tutorials-debugging-qtcreator-components Debug Components

- Load/Create the ArmarX project in QtCreator.
- Set breakpoints to the methods you would like to inspect.
- Start the component.
- In QtCreator select Debug->Start Debugging->Attach to  Running External Application. Then select the component you want to debug.
- Wait until QtCreator is ready again and press play to re-enter the normal execution.
- The execution stops at the breakpoints you selected.

If you need to inspect the startup routines of your component you need to start the component via QtCreator.
Using the attaching method (as described above) you are not able to stop at breakpoints in the initialization routines (Constructor/onCraeteCompontent, etc).
If the component requires additional configuration files you need to add this information to the startup configuration.
Go to the "Projects" pane on the left and select the "Run Settings" properties page on the top.
Select the component you want to debug and edit the startup settings (Run->Arguments) and
add the following to the "Arguments" tab:

\code
--Ice.Config=YOUR_CONFIG_FILE.cfg
\endcode

Now you can start your scenario as usual, but disable the component you want to debug in the startscript. Start the debugging of your component with QtCreator (play button wth bug, be sure the correct application is selected).

\subsection ArmarXCore-Tutorials-debugging-qtcreator-armarx-guis Debug ArmarXGui plugins

ArmarXGui plugins can be debugged similar to the methods described above.
You can start the ArmarX GUI, attach to the ArmarXGui process with QtCreator and set your breakpoints.
When you then load the gui plugin via the ArmarXGui menu, you are able to debug the methods of the plugin.

*/
