/**
\page ArmarXCore-Tutorials-new-robot-kinsim New Robot Tutorial 2: Starting the kinematic simulation

\tableofcontents

This tutorial will expand upon the \ref ArmarXCore-Tutorials-new-robot-sim "previous tutorial" by adding a new scenario with a kinematic simulation.
This will allow you to show and move your robot's joints.

We start off by creating a new scenario with scenario manager (ArmarXGui: ```Add Widger -> Meta -> ScenarioManager```).
Make sure to select the select the package that you created in the first tutorial.
\image html Tutorials-new-robot-kin-create-scenario.png

Add the applications KinematicUnitSimulation and RobotStateComponent to the newly created scenario using the application database.
Now you can configure these applications by setting the following properties:

<table>
        <tr><td>Application</td><td>Properties to set</td></tr>

<tr><td>KinematicUnitSimulation</td><td><pre>
ArmarX.KinematicUnitSimulation.RobotFileName = YouBotTutorial/YouBot.xml
ArmarX.KinematicUnitSimulation.RobotNodeSetName = Joints_Revolute
</pre></td></tr>

<tr><td>RobotStateComponent</td><td><pre>
ArmarX.RobotStateComponent.AgentName = youBot
ArmarX.RobotStateComponent.RobotFileName = YouBotTutorial/YouBot.xml
ArmarX.RobotStateComponent.RobotNodeSetName = Joints_Revolute
</pre></td></tr>
</table>
\note Again, make sure to replace all references to the YouBot.xml file with your robot definition file in the simox format
\note You'll also note that we use the RobotNodeSetName 'Joints_Revolute' here. This is the nodeset we will manipulate via the GUI shortly. Adjust if necessary.

To continue, start the scenario using the ArmarXGui or the CLI:
\verbatim
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario start YouBotKinematicSimulation -p YouBotTutorial
\endverbatim
and add the KinematicSimulation widget via ```Add Widget -> RobotControl -> KinematicUnitGUI```. You should see the widget settings:
\image html Tutorials-new-robot-kinsim-settings.png

You'll need to adjust the 'Robot model filepath' to use your robot, and the 'Robot nodeset name' to the nodeset you want to use.
\note It may take a few seconds before the kinematic unit proxy connects to the widget.

After pressing OK, you should be able to see the actual widget with a 3D view of your robot. You can now also select and move the robot's joints with position or velocity control.

\image html Tutorials-new-robot-kinsim.png
*/
