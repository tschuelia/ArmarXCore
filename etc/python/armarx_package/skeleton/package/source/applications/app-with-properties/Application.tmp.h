/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::application::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_@PACKAGE_NAME@_@COMPONENT_NAME@_H
#define _ARMARX_APPLICATION_@PACKAGE_NAME@_@COMPONENT_NAME@_H


// #include <@PACKAGE_NAME@/components/@MyComponent@/@MyComponent@.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>


namespace armarx
{
    /**
     * @class ApplicationPropertyDefinitions
     * @brief Application property definition container.
     *
     * @see properties
     */
    class ARMARXCORE_IMPORT_EXPORT @COMPONENT_NAME@PropertyDefinitions:
        public ApplicationPropertyDefinitions
    {
    public:
        /**
         * @see armarx::ApplicationPropertyDefinitions
         */
        ApplicationPropertyDefinitions(std::string prefix):
            ApplicationPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("MyOptionalProperty",
                                                "itsDefaultValue",
                                                "What it is used for");

            defineRequiredProperty<std::string>("MyOptionalProperty",
                                                "Its description");
        }
    };


    /**
     * @class @COMPONENT_NAME@App
     * @brief A brief description
     *
     * Detailed Description
     */
    class @COMPONENT_NAME@App :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject( Component::create<@COMPONENT_NAME@>(properties) );
        }


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return PropertyDefinitionsPtr(
                       new @COMPONENT_NAME@PropertyDefinitions(getDomainName()));
        }
    };
}

#endif
