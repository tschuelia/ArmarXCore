
armarx_set_target("@COMPONENT_NAME@GuiPlugin")

find_package(Qt4 COMPONENTS QtCore QtGui QtDesigner)

armarx_build_if(QT_FOUND "Qt not available")
# ArmarXGui gets included through depends_on_armarx_package(ArmarXGui "OPTIONAL")
# in the toplevel CMakeLists.txt
armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")


# all include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
if(QT_FOUND)
    include(${QT_USE_FILE})
endif()

set(SOURCES
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@GuiPlugin.cpp @COMPONENT_PATH@/@COMPONENT_NAME@WidgetController.cpp
)

set(HEADERS
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@GuiPlugin.h @COMPONENT_PATH@/@COMPONENT_NAME@WidgetController.h
)

set(GUI_MOC_HDRS ${HEADERS})

set(GUI_UIS
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@Widget.ui
)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS ${QT_LIBRARIES})

if(ArmarXGui_FOUND)
	armarx_gui_library(@COMPONENT_NAME@GuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
