##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

import re


class TemplateReplacementStrategy(object):

    @classmethod
    def replaceTemplates(self, content):
        # replace each #@TEMPLATE_LINE@REST_OF_LINE with the following two lines where the variables of the first one will be replaced
        # REST_OF_LINE
        # #@TEMPLATE_LINE@REST_OF_LINE
        return re.sub(r"(#@TEMPLATE_LINE@(.*))", r"\g<2>\n\g<1>", content)

    @classmethod
    def getStrategyForComponentType(cls, componentType, strategyName):
        if strategyName == "xmlonly":
            return NoCppXmlStateReplacementStrategy
        elif strategyName == "cpponly":
            return CppOnlyXmlStateReplacementStrategy
        else:
            if componentType == "xmlstate-xmlonly":
                return NoCppXmlStateReplacementStrategy
            return cls


class NoCppXmlStateReplacementStrategy(TemplateReplacementStrategy):
    @classmethod
    def replaceTemplates(self, content):
        "duplicate any line that only begins with #@TEMPLATE_LINE@ and ends with .xml"
        return re.sub(r"(#@TEMPLATE_LINE@(.*)\.xml)", r"\g<2>.xml\n\g<1>", content)


class CppOnlyXmlStateReplacementStrategy(TemplateReplacementStrategy):
    @classmethod
    def replaceTemplates(self, content):
        "duplicate any line that begins with #@TEMPLATE_LINE@ and does not end with .xml"
        return re.sub(r"(#@TEMPLATE_LINE@((?!.*xml).*))", r"\g<2>\n\g<1>", content)
