##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


__author__ = 'kroehner'

import argparse
import os.path, os
from subprocess import call

from armarx import ArmarXBuilder

from Command import Command
import tempfile

class Memory(Command):

    commandName = "memory"

    requiredArgumentCount = 1

    requiresIce = False

    parser = argparse.ArgumentParser(description='Start/Stop/Repair the MemoryX database')

    def __init__(self, profile):
        super(Memory, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('mongod_command', choices = ['start', 'stop', 'repair', 'assureRunning','importDefaultSnapshot'], help='Start/Stop/Repair or import the MemoryX database')

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False)
        packageData = builder.getArmarXPackageData('MemoryX')

        if not len(packageData):
            print 'Could not find the MemoryX package!'
            return 1

        memoryx_binary_dir = builder.getArmarXPackageDataValue(packageData, 'BINARY_DIR')
        if memoryx_binary_dir is None:
            print 'Could not read binary directory from the package cmake data!'
            return 1

        result = 0
        if args.mongod_command == "importDefaultSnapshot":
            tmpPath = tempfile.mkdtemp()
            cloneCmdString = "cd "  + tmpPath + " && git clone https://gitlab.com/ArmarX/ArmarXDB.git "
            print cloneCmdString
            result = os.system(cloneCmdString)
            if result == 1:
                print ("Cloning of the ArmarXDB repository from gitlab failed")
                return 1
            mongoimport_executable = os.path.join(memoryx_binary_dir[0], 'mongoimport.sh')
            mongoimportCmdString = mongoimport_executable + "  " + tmpPath + "/ArmarXDB/data/ArmarXDB/dbexport/memdb"
            print mongoimportCmdString
            result = os.system(mongoimportCmdString)
        else:
            mongod_executable = os.path.join(memoryx_binary_dir[0], 'mongod.sh')
            print "Running " + mongod_executable + ' ' + args.mongod_command
            result = os.system(mongod_executable + ' ' + args.mongod_command)
        if result == 0:
            return 0
        else:
            return 1

    @classmethod
    def getHelpString(cls):
        return "start/Stop/Repair the the MemoryX database"
