##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
# @date       2017
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
from Command import Command

__author__ = 'waechter'


class KillAll(Command):

    commandName = "killAll"
    requiresIce = False

    requiredArgumentCount = 0

    def __init__(self, profile):
        super(KillAll, self).__init__( profile)

    def execute(self, args):
        self.killAll()

    @classmethod
    def killAll(cls):
        try:
            import psutil
        except:
            print cls.commandName + " command not available since 'psutil' module is missing"
            return
        pids = psutil.get_pid_list()
        exceptionList = ["ArmarXGuiRun"]
        killed = False
        for pid in pids:
                p = psutil.Process(pid)
                if p.name.endswith("Run") and p.name not in exceptionList:
                    print "Killing " + p.name
                    p.kill()
                    killed = True

        if not killed:
            print "No applications to kill"

    @classmethod
    def getHelpString(cls):
        return "Kills all local ArmarX processes except the ArmarX GUI (to be more precise every " \
               "process that ends on 'Run')"
