/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>


// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

using namespace armarx;

StringValueMap::StringValueMap(bool forceSingleTypeMap) :
    forceSingleTypeMap(forceSingleTypeMap)
{
    typeContainer = VariantType::Map(VariantType::Invalid).clone();
}

StringValueMap::StringValueMap(VariantTypeId subType)
{
    this->typeContainer = VariantType::Map(subType).clone();
    //    type = Variant::addTypeName(getTypePrefix() + Variant::typeToString(subType));
}

armarx::StringValueMap::StringValueMap(const ContainerType& subType)
{
    this->typeContainer = VariantType::Map(subType).clone();
}

StringValueMap::StringValueMap(const StringValueMap& source) :
    IceUtil::Shared(source),
    VariantContainerBase(source),
    StringValueMapBase(source)
{
    *this = source;
}



StringValueMap& StringValueMap::operator =(const StringValueMap& source)
{

    this->typeContainer = ContainerTypePtr::dynamicCast(source.typeContainer->clone());
    elements.clear();
    StringVariantContainerBaseMap::const_iterator it = source.elements.begin();

    for (; it != source.elements.end(); it++)
    {
        elements[it->first] = it->second->cloneContainer();
    }

    return *this;
}

VariantContainerBasePtr StringValueMap::cloneContainer(const Ice::Current& c) const
{
    VariantContainerBasePtr result = new StringValueMap(*this);

    return result;
}

Ice::ObjectPtr StringValueMap::ice_clone() const
{
    return this->clone();
}

void StringValueMap::addElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c)
{
    if (elements.find(key) != elements.end())
    {
        throw KeyAlreadyExistsException();
    }

    setElement(key, variantContainer->cloneContainer());
}

void StringValueMap::addVariant(const std::string& key, const Variant& variant)
{
    setElement(key, new SingleVariant(variant));
}

void StringValueMap::setElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c)
{
    if (forceSingleTypeMap && !VariantContainerType::compare(variantContainer->getContainerType(), getContainerType()->subType)
        && getContainerType()->subType->typeId != Variant::typeToString(VariantType::Invalid))
    {
        throw exceptions::user::InvalidTypeException(getContainerType()->subType->typeId, variantContainer->getContainerType()->typeId);
    }

    if (getContainerType()->subType->typeId == Variant::typeToString(VariantType::Invalid))
    {
        getContainerType()->subType = variantContainer->getContainerType()->clone();
    }

    elements[key] = (variantContainer->cloneContainer());
}

void StringValueMap::clear(const Ice::Current& c)
{
    elements.clear();
}



VariantTypeId StringValueMap::getStaticType(const Ice::Current& c)
{
    return Variant::addTypeName(getTypePrefix());
}



int StringValueMap::getSize(const Ice::Current& c) const
{
    return int(elements.size());
}

bool StringValueMap::validateElements(const Ice::Current& c)
{
    StringVariantContainerBaseMap::iterator it = elements.begin();
    bool result = true;

    for (; it != elements.end(); it++)
    {
        result = result && it->second->validateElements();
    }

    return result;
}

VariantContainerBasePtr StringValueMap::getElementBase(const std::string& key, const Ice::Current& c) const
{
    StringVariantContainerBaseMap::const_iterator it = elements.find(key);

    if (it == elements.end())
    {
        throw IndexOutOfBoundsException();
    }

    return it->second;
}

VariantPtr StringValueMap::getVariant(const std::string& key) const
{
    VariantPtr ptr = getElement<SingleVariant>(key)->get();

    if (!ptr)
    {
        throw InvalidTypeException();
    }

    return ptr;
}

std::string StringValueMap::getTypePrefix()
{
    return "::armarx::StringValueMapBase";
}


std::string StringValueMap::toString(const Ice::Current& c) const
{
    std::stringstream ss;

    for (const std::pair<std::string, VariantContainerBasePtr>& element : elements)
    {
        ss << element.first << ": " << element.second->toString() << "\n";
    }

    return ss.str();


}

void StringValueMap::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr newObj = obj->createElement();

    StringVariantContainerBaseMap::const_iterator it = elements.begin();

    for (; it != elements.end(); it++)
    {
        newObj->setIceObject(it->first, it->second);
    }

    //    obj->setString("type", ice_id());
    obj->setElement("map", newObj);
}

void StringValueMap::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr map = obj->getElement("map");
    Ice::StringSeq keys = map->getElementNames();

    for (Ice::StringSeq::iterator it = keys.begin(); it != keys.end(); it++)
    {
        VariantContainerBasePtr c = VariantContainerBasePtr::dynamicCast(map->getIceObject(*it));

        if (c)
        {
            addElement(*it, c);
        }
        else
        {
            throw LocalException("Could not cast to VariantContainerBasePtr");
        }
    }
}




Ice::Int armarx::StringValueMap::getType(const Ice::Current&) const
{
    return StringValueMap::getStaticType();
}
