/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::observers::ProfilerObserver
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_PROFILER_OBSERVER_H
#define _ARMARX_CORE_PROFILER_OBSERVER_H

#include <ArmarXCore/interface/observers/ProfilerObserverInterface.h>

#include "../observers/Observer.h"
#include "../observers/variant/StringValueMap.h"

namespace armarx
{
    /**
     * @defgroup Component-ProfilerObserver ProfilerObserver
     * @ingroup ObserversSub ArmarXCore-Components
     * The ProfilerObserver class listens on the topic armarx::Profiler::PROFILER_TOPIC_NAME (defined in Core/interface/core/Profiler.ice) and provides the data via Channels.
     *
     * Network profiling can be enabled for any armarx::Application by setting the property armarx::NetworkStats to "1".
     * This property can either be set in ~/.armarx/default.cfg or on the commandline.
     *
     * The gathered data can for example be viewed with the Logger plugin of ArmarXGui.
     *
     * @class ProfilerObserver
     * @ingroup Component-ProfilerObserver
     */
    class ProfilerObserver :
        virtual public Observer,
        virtual public armarx::ProfilerObserverInterface
    {
    public:
        ~ProfilerObserver();
        virtual std::string getDefaultName() const;
        void onInitObserver();
        void onConnectObserver();

        void reportNetworkTraffic(const std::string& id, const std::string& protocol, Ice::Int inBytes, Ice::Int outBytes, const Ice::Current& = ::Ice::Current());
        void reportEvent(const ProfilerEvent& event, const Ice::Current& = ::Ice::Current());

        void reportStatechartTransition(const ProfilerStatechartTransition& transition, const Ice::Current& = ::Ice::Current());
        void reportStatechartInputParameters(const ProfilerStatechartParameters& inputParametes, const Ice::Current& = ::Ice::Current());
        void reportStatechartLocalParameters(const ProfilerStatechartParameters& localParameters, const Ice::Current& = ::Ice::Current());
        void reportStatechartOutputParameters(const ProfilerStatechartParameters& outputParameters, const Ice::Current& = ::Ice::Current());

        void reportProcessCpuUsage(const ProfilerProcessCpuUsage& process, const Ice::Current& = ::Ice::Current());
        void reportProcessMemoryUsage(const ProfilerProcessMemoryUsage& memoryUsage, const Ice::Current& = ::Ice::Current());


        void reportEventList(const ProfilerEventList& events, const Ice::Current& = ::Ice::Current());

        void reportStatechartTransitionList(const ProfilerStatechartTransitionList& transitions, const Ice::Current& = ::Ice::Current());
        void reportStatechartInputParametersList(const ProfilerStatechartParametersList& inputParametersList, const ::Ice::Current& = ::Ice::Current());
        void reportStatechartLocalParametersList(const ProfilerStatechartParametersList& localParametersList, const Ice::Current& = ::Ice::Current());
        void reportStatechartOutputParametersList(const ProfilerStatechartParametersList& outputParametersList, const Ice::Current& = ::Ice::Current());

        void reportProcessCpuUsageList(const ProfilerProcessCpuUsageList& processes, const Ice::Current& = ::Ice::Current());
        void reportProcessMemoryUsageList(const ProfilerProcessMemoryUsageList& memoryUsages, const Ice::Current& = ::Ice::Current());
    private:
        /**
         * @brief createStateChannelIfRequired if channel with @p channelName does not exist, create it and all required datafields
         * @param channelName
         */
        void createStateChannelIfRequired(const std::string& channelName);
        /**
         * @brief createResourceChannelIfRequired if channel with @p channelName does not exist, create it and all required datafields
         * @param channelName
         */
        void createResourceChannelIfRequired(const std::string& channelName);

        static void StateParameterToVariantMap(const armarx::StateParameterMap& parameterMap, armarx::StringValueMap& variantMap);
        Mutex channelCheckMutex;
    };
}

#endif
