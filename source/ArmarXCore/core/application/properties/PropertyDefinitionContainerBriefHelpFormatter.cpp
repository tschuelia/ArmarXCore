/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <IceUtil/Handle.h>             // for HandleBase, Handle
#include <boost/program_options/errors.hpp>  // for program_options
#include <boost/program_options/options_description.hpp>
#include <list>                         // for _List_const_iterator, etc
#include <sstream>                      // for stringstream, ostream, etc

#include "ArmarXCore/core/application/properties/PropertyDefinitionContainer.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyUser.h"
#include "PropertyDefinitionContainerBriefHelpFormatter.h"

namespace armarx
{
    class PropertyDefinitionFormatter;
}  // namespace armarx

using namespace armarx;

namespace po = boost::program_options;

PropertyDefinitionContainerBriefHelpFormatter::PropertyDefinitionContainerBriefHelpFormatter(
    PropertyDefinitionFormatter& defFormatter,
    po::options_description* optionsDescription):
    PropertyDefinitionContainerFormatter(defFormatter)
{
    this->optionsDescription = optionsDescription;
}

std::string PropertyDefinitionContainerBriefHelpFormatter::formatPropertyUsers(
    const PropertyUserList& propertyUserList)
{
    std::stringstream ss;

    for (PropertyUserList::const_iterator it = propertyUserList.begin();
         it != propertyUserList.end();
         ++it)
    {
        ss << (*it)->getPropertyDefinitions()->toString(defFormatter);
    }

    ss << *optionsDescription << std::endl;

    return ss.str();
}
