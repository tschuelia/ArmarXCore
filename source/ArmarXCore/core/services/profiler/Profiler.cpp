/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Profiler.h"
#include "LoggingStrategy.h"
#include "IceLoggingStrategy.h"
#include <boost/lexical_cast.hpp>

std::string armarx::Profiler::Profiler::GetEventName(armarx::Profiler::Profiler::EventType eventType)
{
    return Profiler::GetEventTypeMap()[eventType];
}


armarx::Profiler::Profiler::Profiler() :
    profilerName(boost::lexical_cast<std::string>(getpid())),
    logger(new LoggingStrategy),
    startTime(IceUtil::Time::now()),
    timestampUnit("ms")
{
}


armarx::Profiler::Profiler::~Profiler()
{
}


void armarx::Profiler::Profiler::setName(const std::string& profilerName)
{
    this->profilerName = profilerName;
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->setId(profilerName);
}

void armarx::Profiler::Profiler::setLoggingStrategy(LoggingStrategyPtr loggingStrategy)
{
    if (!loggingStrategy)
    {
        return;
    }

    boost::mutex::scoped_lock lock(loggerMutex);
    logger = loggingStrategy;
    logger->setId(profilerName);
}


void armarx::Profiler::Profiler::logEvent(armarx::Profiler::Profiler::EventType eventType, const std::string& parentName, const std::string& functionName)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logEvent(::getpid(), getTimestampMilliseconds(), profilerName, timestampUnit, Profiler::GetEventName(eventType), parentName, functionName);
}


void armarx::Profiler::Profiler::logStatechartTransition(const std::string& parentStateIdentifier, const std::string& sourceStateIdentifier, const std::string& targetStateIdentifier, const std::string& eventName)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logStatechartTransition(::getpid(), getTimestampMilliseconds(), parentStateIdentifier, sourceStateIdentifier, targetStateIdentifier, eventName);
}

void armarx::Profiler::Profiler::logStatechartInputParameters(const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logStatechartInputParameters(::getpid(), getTimestampMilliseconds(), stateIdentifier, inputParameterMap);
}

void armarx::Profiler::Profiler::logStatechartLocalParameters(const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logStatechartLocalParameters(::getpid(), getTimestampMilliseconds(), stateIdentifier, localParameterMap);
}

void armarx::Profiler::Profiler::logStatechartOutputParameters(const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logStatechartOutputParameters(::getpid(), getTimestampMilliseconds(), stateIdentifier, outputParameterMap);
}

void armarx::Profiler::Profiler::logProcessCpuUsage(float cpuUsage)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logProcessCpuUsage(::getpid(), getTimestampMilliseconds(), cpuUsage);
}

void armarx::Profiler::Profiler::logProcessMemoryUsage(int memoryUsage)
{
    boost::mutex::scoped_lock lock(loggerMutex);
    logger->logProcessMemoryUsage(::getpid(), getTimestampMilliseconds(), memoryUsage);
}


IceUtil::Int64 armarx::Profiler::Profiler::getTimestampMilliseconds()
{
    return (IceUtil::Time::now() - startTime).toMilliSeconds();
}


void armarx::Profiler::Profiler::reset()
{
    startTime = IceUtil::Time::now();
}



armarx::Profiler::Profiler::EventTypeMap armarx::Profiler::Profiler::evenTypeNameMap;


armarx::Profiler::Profiler::EventTypeMap& armarx::Profiler::Profiler::GetEventTypeMap()
{
    if (!Profiler::evenTypeNameMap.empty())
    {
        return Profiler::evenTypeNameMap;
    }

    Profiler::evenTypeNameMap[Profiler::eFunctionStart] = "FUNCTION_START";
    Profiler::evenTypeNameMap[Profiler::eFunctionReturn] = "FUNCTION_RETURN";
    Profiler::evenTypeNameMap[Profiler::eFunctionBreak] = "FUNCTION_BREAK";
    return Profiler::evenTypeNameMap;
}
