#ifndef ARMARX_PROFILER_ICE_LOGGING_STRATEGY_H
#define ARMARX_PROFILER_ICE_LOGGING_STRATEGY_H

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/interface/core/Profiler.h>
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <boost/thread/mutex.hpp>
#include <boost/thread/pthread/mutex.hpp>  // for mutex
#include <stdint.h>                     // for uint64_t
#include <sys/types.h>                  // for pid_t, u_int64_t
#include <ostream>                      // for operator<<
#include <string>                       // for string, operator<<
#include <vector>                       // for vector

#include "../../services/tasks/PeriodicTask.h"  // for PeriodicTask, etc
#include "ArmarXCore/interface/statechart/StatechartIce.h"
#include "LoggingStrategy.h"            // for LoggingStrategy

namespace armarx
{
    namespace Profiler
    {
        class IceLoggingStrategy;

        typedef boost::shared_ptr<IceLoggingStrategy> IceLoggingStrategyPtr;

        /**
         * @class IceLoggingStrategy
         * @ingroup Profiling
         * @brief IceLoggingStrategy publishes incoming log method calls directly on IceLoggingStrategy::profilerListenerPrx.
         *
         * Instances of this strategy object is used by armarx::Profiler.
         */
        class IceLoggingStrategy :
            virtual public LoggingStrategy
        {
        public:
            IceLoggingStrategy(ProfilerListenerPrx profilerTopic);

            virtual ~IceLoggingStrategy();

            virtual void logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName);

            virtual void logStatechartTransition(pid_t processId, uint64_t timestamp, const std::string& parentStateIdentifier, const std::string& sourceStateIdentifier, const std::string& targetStateIdentifier, const std::string& eventName);
            virtual void logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap);
            virtual void logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap);
            virtual void logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap);

            virtual void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage);
            virtual void logProcessMemoryUsage(pid_t processId, u_int64_t timestamp, int memoryUsage);
        protected:
            ProfilerListenerPrx profilerListenerPrx;
        };


        /**
         * @class IceBufferdLoggingStrategy
         * @ingroup Profiling
         * @brief IceBufferdLoggingStrategy buffers incoming log method calls and publishes them as collections on IceLoggingStrategy::profilerListenerPrx.
         *
         * Instances of this strategy object is used by armarx::Profiler.
         */
        class IceBufferedLoggingStrategy :
            virtual public LoggingStrategy
        {
        public:
            IceBufferedLoggingStrategy(ProfilerListenerPrx profilerTopic);

            virtual ~IceBufferedLoggingStrategy();

            virtual void logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName);

            virtual void logStatechartTransition(pid_t processId, uint64_t timestamp, const std::string& parentStateIdentifier, const std::string& sourceStateIdentifier, const std::string& targetStateIdentifier, const std::string& eventName);
            virtual void logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap);
            virtual void logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap);
            virtual void logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap);

            virtual void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage);
            virtual void logProcessMemoryUsage(pid_t processId, u_int64_t timestamp, int memoryUsage);
        protected:
            void publishData();
            armarx::StateParameterMap copyDictionary(const armarx::StateParameterMap& source);


            boost::mutex profilerEventsMutex;
            ProfilerEventList profilerEvents;

            boost::mutex profilerStatechartTransitionsMutex;
            ProfilerStatechartTransitionList profilerStatechartTransitions;

            boost::mutex profilerStatechartInputParametersMutex;
            ProfilerStatechartParametersList profilerStatechartInputParameters;

            boost::mutex profilerStatechartLocalParametersMutex;
            ProfilerStatechartParametersList profilerStatechartLocalParameters;

            boost::mutex profilerStatechartOutputParametersMutex;
            ProfilerStatechartParametersList profilerStatechartOutputParameters;

            boost::mutex profilerCpuUsagesMutex;
            ProfilerProcessCpuUsageList profilerProcessCpuUsages;

            boost::mutex profilerProcessMemoryUsagesMutex;
            ProfilerProcessMemoryUsageList profilerProcessMemoryUsages;

            ProfilerListenerPrx profilerListenerPrx;

            PeriodicTask<IceBufferedLoggingStrategy>::pointer_type publisherTask;
        };
    }
}

#endif
