/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_CORE_TEMPLATEMETAPROGRAMMING_H
#define _ARMARX_CORE_TEMPLATEMETAPROGRAMMING_H

#include <type_traits>
#include <tuple>

namespace armarx
{
    namespace meta
    {

        /// @brief Helper for sfinae (is added in c++17)
        template<class...>
        using void_t = void;

        /**
         * @brief Traits about templates taking only types as parameters.
         *
         * \ingroup core-utility
         */
        struct TypeTemplateTraits
        {
            /**
             * @brief Whether a type T is the instance of a given template Template. (this is the case for false)
             */
            template<template<class...> class Template, class T       > struct IsInstanceOf                                : std::false_type {};
            /**
             * @brief Whether a type T is the instance of a given template Template. (this is the case for true)
             */
            template<template<class...> class Template, class...Params> struct IsInstanceOf<Template, Template<Params...>> : std:: true_type {};
        };

        /**
         * @brief Get the type of the last element of a template parameter pack.
         */
        template<class...Ts>
        using last_type = typename std::decay < typename std::tuple_element < sizeof...(Ts) - 1, std::tuple<Ts... >>::type >::type;


        /**
         * @brief Can be used to determine if there is an overload for std::to_string for a type T
         *
         * This is the default if there is no overload
         */
        template<class T, class = void>
        struct HasToString : std::false_type {};

        /**
         * @brief Can be used to determine if there is an overload for std::to_string for a type T
         *
         * This is the specialization if there is an overload.
         */
        template<class T>
struct HasToString < T, typename std::enable_if < std::is_same < decltype(std::to_string(std::declval<T>()), int {}), int >::value >::type > :
        std::true_type {};
        //can't use this since gcc is broken / sfinae is bad in c++11
        //struct HasToString < T, void_t<decltype(std::to_string(std::declval<T>()))> > :std::true_type {};


        /**
         * @brief Can be used to determine if T has an at method accepting a type of IdxT
         *
         * This is the default if there is no method
         */
        template<class T, class IdxT, class = void>
        struct HasAtMethod : std::false_type {};

        /**
         * @brief Can be used to determine if T has an at method accepting a type of IdxT
         *
         * This is the specialization if there is a method
         */
        template<class T, class IdxT>
struct HasAtMethod < T, IdxT, typename std::enable_if < std::is_same < decltype(std::declval<T>().at(std::declval<IdxT>()), int {}), int >::value >::type > :
        std::true_type {};
        //can't use this since gcc is broken / sfinae is bad in c++11
        //struct HasAtMethod< T, IdxT,void_t<decltype(std::declval<T>().at(std::declval<IdxT>()))>> : std::true_type {};

#define ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(CHECKNAME, FNCNAME, FNCSIG)   \
    template <class T>                                                      \
    class CHECKNAME                                                         \
    {                                                                       \
        struct one {};                                                      \
        struct two { one a, b; };                                           \
        template<class C, C> struct classAndFncPtr;                         \
        template<class C>                                                   \
        static one has(classAndFncPtr<FNCSIG, &C::FNCNAME>*);               \
        template<class T2>                                                  \
        static two has(...);                                                \
    public:                                                                 \
        enum e {value = sizeof(has<T>(0)) == sizeof(one)};                  \
    }

    }
}
#endif
