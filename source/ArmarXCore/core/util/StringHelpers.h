/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_STRINGHELPERS_H
#define _ARMARX_STRINGHELPERS_H

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <unordered_map>
#include <ArmarXCore/core/system/Synchronization.h>
#include "TemplateMetaProgramming.h"
#include <cxxabi.h>
//#include <boost/algorithm/string/split.hpp>
#include <boost/units/detail/utility.hpp>


namespace armarx
{
    /**
     * @brief Converts a string to float and uses always *dot* as seperator.
     */
    float toFloat(const std::string& input);

    template <typename T>
    std::string ValueToString(const T& value)
    {
        std::stringstream str;
        str << value;
        return str.str();
    }

    template <typename T>
    std::string GetTypeString(bool withoutNamespaceSpecifier = false)
    {
        static const std::string demangled = boost::units::detail::demangle(typeid(T).name());
        static const auto removeNamespaceSpecifier = [](const std::string & demangled)
        {
            std::size_t substrStart = 0;
            std::size_t level = 0;
            for (int i = static_cast<int>(demangled.size() - 1); i >= 0; --i)
            {

                if (!level && demangled.at(i) == ':')
                {
                    substrStart = i + 1;
                    break;
                }
                level += (demangled.at(i) == '>') - (demangled.at(i) == '<');
            }
            return demangled.substr(substrStart);
        };
        static const std::string demangledWithoutNamespaceSpecifier = removeNamespaceSpecifier(demangled);
        return withoutNamespaceSpecifier ? demangledWithoutNamespaceSpecifier : demangled;
    }

    template <typename T>
    std::string GetTypeString(const T&, bool withoutNamespaceSpecifier = false)
    {
        return GetTypeString<T>(withoutNamespaceSpecifier);
    }

    bool Contains(const std::string& haystack, const std::string& needle, bool caseInsensitive = false);
    std::vector<std::string> Split(const std::string& source, const std::string& splitBy, bool trimElements = false, bool removeEmptyElements = false);


    inline void EncodeInline(std::string& data)
    {
        std::string buffer;
        buffer.reserve(data.size());
        for (size_t pos = 0; pos != data.size(); ++pos)
        {
            switch (data[pos])
            {
                case '&':
                    buffer.append("&amp;");
                    break;
                case '\"':
                    buffer.append("&quot;");
                    break;
                case '\'':
                    buffer.append("&apos;");
                    break;
                case '<':
                    buffer.append("&lt;");
                    break;
                case '>':
                    buffer.append("&gt;");
                    break;
                default:
                    buffer.append(&data[pos], 1);
                    break;
            }
        }
        data.swap(buffer);
    }

    std::string Encode(const std::string& data);
}
namespace std
{
    template <typename T>
    std::string& operator <<(std::string& str, const T& value)
    {
        str += armarx::ValueToString(value);
        return str;
    }

    template<typename T>
    ostream&
    operator<<(ostream& str, const std::vector<T>& vector)
    {
        str << "Vector(" << vector.size() << "):\n";

        for (unsigned int i = 0; i < vector.size(); ++i)
        {
            str << "\t(" << i << "):" << vector.at(i) << "\n";
        }

        return str;
    }

    template<typename T1, typename T2>
    ostream&
    operator<<(ostream& str, const std::map<T1, T2>& map)
    {
        typedef std::map<T1, T2> MapType;

        str << "Map<" << armarx::GetTypeString<T1>() << ", " <<  armarx::GetTypeString<T2>() << ">(" << map.size() << "):\n";

        for (typename MapType::const_iterator i = map.begin(); i != map.end(); ++i)
        {
            str << "\t" << i->first << ": " << i->second << "\n";
        }

        return str;
    }

    template<typename T1, typename T2>
    ostream&
    operator<<(ostream& str, const std::unordered_map<T1, T2>& map)
    {
        typedef std::unordered_map<T1, T2> MapType;

        str << "Unordered Map<" << armarx::GetTypeString<T1>() << ", " <<  armarx::GetTypeString<T2>() << ">(" << map.size() << "):\n";

        for (typename MapType::const_iterator i = map.begin(); i != map.end(); ++i)
        {
            str << "\t" << i->first << ": " << i->second << "\n";
        }

        return str;
    }
}

#define VAROUT(x) std::string(std::string(#x) +": " + armarx::ValueToString(x)) + " "

#endif
