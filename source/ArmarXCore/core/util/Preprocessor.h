/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_CORE_PREPROCESSOR_H
#define _ARMARX_CORE_PREPROCESSOR_H

/**
 * \defgroup CPreprocessor
 * \ingroup core-utility
 */

#ifndef __COUNTER__
#pragma message "The macro __COUNTER__ is not defined! It will be defined as MACRO_COUNTER_NOT_DEFINED"
#define __COUNTER__ MACRO_COUNTER_NOT_DEFINED
#endif


/**
 * @brief Expands the given parameters and stringifies them
 * \ingroup CPreprocessor
 */
#define ARMARX_STRINGIFY(...) ARMARX_STRINGIFY_IMPL(__VA_ARGS__)
#define ARMARX_STRINGIFY_IMPL(...) #__VA_ARGS__

/**
 * @brief concatenates its 5 parameters to one token (macros are not expanded)
 * Use ARMARX_CONCATENATE_5 instead!
 * @see ARMARX_CONCATENATE_5
 */
#define ARMARX_CONCATENATE_5_IMPL(s1, s2, s3, s4, s5) s1##s2##s3##s4##s5

/**
 * @brief concatenates its 5 parameters to one token (macros are expanded)
 * \ingroup CPreprocessor
 */
#define ARMARX_CONCATENATE_5(s1, s2, s3, s4, s5) ARMARX_CONCATENATE_5_IMPL(s1, s2, s3, s4, s5)

/**
 * @brief creates a identifier with a given prefix.
 * The prefix may help when the variable shows up in compiler output.
 *
 * can be used like this:
 * \code
 *  void foo()
 *  {
 *      std::mutex mtx;
 *      std::lock_guard<std::mutex> ARMARX_ANONYMOUS_VARIABLE_WITH_PREFIX(guard)(mtx);
 *      //guarded section
 *  }
 * \endcode
 * @see ARMARX_ANONYMOUS_VARIABLE
 * \ingroup CPreprocessor
 */
#define ARMARX_ANONYMOUS_VARIABLE_WITH_PREFIX(pre) ARMARX_CONCATENATE_5(pre, _IN_LINE_, __LINE__, _WITH_COUNT_, __COUNTER__)

/**
 * @brief creates a identifier.
 * The created identifier contains the linenumber and is unique per translation unit.
 * This makro's usecase are other macros, where a variable for RAII has to be created in the enclosing scope.
 *
 * can be used like this:
 * \code
 *  void foo()
 *  {
 *      std::mutex mtx;
 *      std::lock_guard<std::mutex> ARMARX_ANONYMOUS_VARIABLE(mtx);
 *      //guarded section
 *  }
 * \endcode
 * \ingroup CPreprocessor
 */
#define ARMARX_ANONYMOUS_VARIABLE ARMARX_ANONYMOUS_VARIABLE_WITH_PREFIX(armarxAnonymousVariable)

#endif
