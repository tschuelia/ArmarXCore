
armarx_component_set_name(XMLRemoteStateOfferer)

find_package(jsoncpp QUIET)
armarx_build_if(jsoncpp_FOUND "jsoncpp not available")

if(jsoncpp_FOUND)
    include_directories(${jsoncpp_INCLUDE_DIR})
endif()

#find_package(Simox 2.3.0 QUIET)
#armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ArmarXCoreStatechart)

set(SOURCES main.cpp)

armarx_add_component_executable("${SOURCES}")
