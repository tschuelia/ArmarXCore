
armarx_component_set_name(StatechartGroupDocGeneratorApp)

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ArmarXCoreStatechart)

set(SOURCES main.cpp StatechartGroupDocGeneratorApp.h)

armarx_add_component_executable("${SOURCES}")
