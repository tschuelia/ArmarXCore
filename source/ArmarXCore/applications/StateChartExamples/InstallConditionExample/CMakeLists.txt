
armarx_component_set_name(InstallConditionExample)


set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ArmarXCoreStatechart)

set(SOURCES main.cpp
    InstallConditionExample.cpp
    InstallConditionExample.h
    InstallConditionExampleApp.h)

armarx_add_component_executable("${SOURCES}")
