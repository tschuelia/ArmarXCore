armarx_set_target("Core Library: ArmarXCoreJsonObject")

find_package(jsoncpp QUIET)

armarx_build_if(jsoncpp_FOUND "jsoncpp not available")

if (jsoncpp_FOUND)
    include_directories(${jsoncpp_INCLUDE_DIR})
endif()

set(LIB_NAME       ArmarXCoreJsonObject)



set(LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ${jsoncpp_LIBRARIES})

set(LIB_FILES JSONObject.cpp
              )

set(LIB_HEADERS JSONObject.h
                )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

add_subdirectory(test)
