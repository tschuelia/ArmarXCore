/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PidStopperLinux.h"
#include <sys/types.h>
#include <signal.h>

using namespace ScenarioManager;
using namespace Exec;
using namespace Data_Structure;

void PidStopperLinux::stop(ApplicationInstancePtr application)
{
    if (application->getPid() > 0)
    {
        std::cout << "Stopping application " << application->getName() << std::endl;
        ::kill(application->getPid(), 15);
    }
    else
    {
        std::cout << "Application " << application->getName() << " is not running" << std::endl;
    }
}

void PidStopperLinux::kill(ApplicationInstancePtr application)
{
    if (application->getPid() > 0)
    {
        std::cout << "Killing application " << application->getName() << std::endl;
        ::kill(application->getPid(), 9);
    }
    else
    {
        std::cout << "Application " << application->getName() << " is not running" << std::endl;
    }
}
