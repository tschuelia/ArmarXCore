#ifndef ARMARX_ScenarioManager_PIDMANAGER_H
#define ARMARX_ScenarioManager_PIDMANAGER_H

#include "../data_structure/ApplicationInstance.h"

namespace ScenarioManager
{
    class PidManager
    {
    public:
        PidManager();

        int loadPid(Data_Structure::ApplicationInstancePtr app);
        void savePid(Data_Structure::ApplicationInstancePtr app);

        static void clearCache();
    private:
    };
}

#include "../executor/Executor.h"

#endif
