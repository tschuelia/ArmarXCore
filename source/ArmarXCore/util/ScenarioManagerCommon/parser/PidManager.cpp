#include "PidManager.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <boost/filesystem.hpp>

#include <fstream>

using namespace ScenarioManager;
using namespace Exec;
using namespace armarx;

PidManager::PidManager()
{
}


int PidManager::loadPid(Data_Structure::ApplicationInstancePtr app)
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    if (cachePath.empty())
    {
        return -1;
    }

    cachePath.append("/pids/");
    boost::filesystem::path dir(cachePath);

    if (!boost::filesystem::exists(dir) && !boost::filesystem::create_directories(dir))
    {
        ARMARX_WARNING_S << "Unable to create Cache directory for the ScenarioManager plugin at " << cachePath;
    }


    std::string filename = cachePath
                           + app->getPackageName() + "."
                           + app->getScenario()->getName() +  "."
                           + app->getName() + (app->getInstanceName().empty() ? "" : ".")
                           + app->getInstanceName() + ".pids";

    if (!boost::filesystem::exists(filename))
    {
        return -1;
    }

    std::ifstream input(filename);

    int pid;
    input >> pid;

    return pid;
}

void PidManager::savePid(Data_Structure::ApplicationInstancePtr app)
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    cachePath.append("/pids/");
    boost::filesystem::path dir(cachePath);

    if (!boost::filesystem::exists(dir) && !boost::filesystem::create_directories(dir))
    {
        ARMARX_WARNING_S << "Unable to create Cache directory for the ScenarioManager plugin at " << cachePath;
    }

    std::string filename = cachePath
                           + app->getPackageName() + "."
                           + app->getScenario()->getName() +  "."
                           + app->getName() + (app->getInstanceName().empty() ? "" : ".")
                           + app->getInstanceName() + ".pids";

    if (app->getPid() == -1)
    {
        boost::filesystem::remove(boost::filesystem::path(filename));
        return;
    }

    std::ofstream output(filename);

    output << app->getPid();
    output.close();
}

void PidManager::clearCache()
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    cachePath.append("/pids/");

    boost::filesystem::remove_all(boost::filesystem::path(cachePath));
}
