#ifndef EXTERNALAPPLICATIONMANAGER_INTERFACE_SLICE
#define EXTERNALAPPLICATIONMANAGER_INTERFACE_SLICE

module armarx
{
    interface ExternalApplicationManagerInterface
    {
        void restartApplication();
        void terminateApplication();
        string getPathToApplication();
        bool isApplicationRunning();
    };
};

#endif
