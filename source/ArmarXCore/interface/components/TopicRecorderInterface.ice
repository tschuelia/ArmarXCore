#ifndef TOPICRECORDER_INTERFACE_SLICE
#define TOPICRECORDER_INTERFACE_SLICE

module armarx
{
    interface TopicRecorderInterface
    {
        void startRecording(int maxDuration);
        bool isRecording();
        void stopRecording();
        void setOutputFilename(string newFilename);
    };

    interface TopicRecorderListenerInterface
    {
        void onStartRecording();
    };

    interface TopicReplayerListenerInterface
    {

        void onStartReplay(string filename);
        void onStopReply();

    };
};

#endif
